import csv

def read_csv(fn):
    csv_file_object = csv.reader(open(fn, 'rb'))
    csv_file_object.next()
    data = []
    for row in csv_file_object:
        data.append(row)
    return data

def read_train():
    return read_csv('data/train.csv')

def read_test():
    return read_csv('data/test.csv')

def write_predictions(pred):
    f_out = open('predictions.csv','w')
    f_out.write("PassengerId,Survived\n")
    for pid,y in pred:
        f_out.write("%d,%d\n"%(pid,int(y)))
    f_out.close()

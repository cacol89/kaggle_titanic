import data_io,data_prep
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn import cross_validation
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.tree import DecisionTreeClassifier

from sklearn.feature_selection import RFE
from sklearn.svm import SVR

def get_classifier():
    return DecisionTreeClassifier()
    #return GradientBoostingClassifier(
    #        n_estimators=4000,
    #        max_depth=12
    #    )
    #return RandomForestClassifier(  n_estimators=1000,
    #                                #n_jobs=6,
    #                                #max_features=None,
    #                                #min_samples_split=2,
    #                                #max_depth=None,
    #                                random_state=234525)


def main():
    cross_validate,predict = True,True

    print "fetching and preparing train data..."
    all_train_data = data_io.read_train()

    train_data = [row[2:] for row in all_train_data]
    target = [row[1] for row in all_train_data]

    print "cleaning train data..."
    prep_vars = data_prep.prep_vars()
    train_features = data_prep.prepare(train_data,prep_vars)


    classifier = get_classifier()

    #estimator = SVR(kernel="linear")
    #selector = RFE(estimator, 1, step=1)
    #selector = selector.fit(np.array(train_features), np.array(target))
    #print selector.ranking_
    #return

    if cross_validate:
        print "Cross-validating the classifier..."
        scores = cross_validation.cross_val_score(classifier,train_features,target,cv=10,n_jobs=6)
        print("Accuracy: %0.4f (+/- %0.4f)" % (scores.mean(), scores.std() / 2))

    if predict:
        print "fitting the classifier..."
        classifier.fit(train_features,target)

        print "fetching and preparing test data..."
        all_test_data = data_io.read_test()
        test_data = [row[1:] for row in all_test_data]
        ids = [int(row[0]) for row in all_test_data]

        print "cleaning test data..."
        test_features = data_prep.prepare(test_data,prep_vars)

        print "predicting..."
        pred = classifier.predict(test_features)

        print "writting predictions..."
        data_io.write_predictions(zip(ids,pred))

if __name__=="__main__":
    main()

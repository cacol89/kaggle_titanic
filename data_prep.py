class prep_vars:
    def __init__(self):
        self.sex = {"male":0.0,"female":1.0}
        self.embarked = {"C":0.0,"S":1.0,"Q":2.0,"":3.0}
        self.cabin = {}
        self.cabin_cnt = 0.0

    def cab_code(self,letter):
        if letter not in self.cabin:
            self.cabin_cnt += 1.0
            self.cabin[letter] = self.cabin_cnt
        return self.cabin[letter]

def proc_cabin(cab,prep_vars):
    if cab == "":
        return 0.0
    l = cab.split(" ")[0][0]
    return ord(l)-ord('A')+1

def calc_bin(val,bin_sz,bin_ceil):
    if val >= bin_ceil:
        return int(bin_ceil/bin_sz)
    return int(val/bin_sz)

def lower_bound(x,lst):
    i = 1
    while i<len(lst) and x>=lst[i]:
        i+=1
    return i-1

def clean(row,prep_vars):
    pclass = float(row[0])
    sex = prep_vars.sex[row[2]]
    age = 0.0 if row[3]=="" else float(row[3])
    sibsp = float(row[4])
    parch = float(row[5])
    fare = 0.0 if row[7] == "" else float(row[7])
    cabin = proc_cabin(row[8],prep_vars)
    embarked = prep_vars.embarked[row[9]]

    return [ 
            sex
            #,pclass
            #,age
            #,calc_bin(age,5.0,30.0)
            #,0 if age<=10 else 1
            #,0 if age<50 else 1
            ,1.0 if row[3]=="" else 0.0
            ,lower_bound(age,[0,11,50])
            #,sibsp
            ,parch
            #,calc_bin(sibsp+parch,1.0,4.0)
            #,1 if sibsp>0 and parch>0 else 0
            ,calc_bin(sibsp,1.0,4.0)
            #,calc_bin(parch,1.0,5.0)
            #,0 if sibsp==0 else 1
            #0 if parch==0 else 1
            #,0 if sibsp<2 else 1
            #0 if parch<2 else 1
            #,fare
            ,calc_bin(fare,20.0,80.0)
            #,0 if row[1] == "" else 1
            #,0 if row[6] == "" else 1
            #,0 if row[7] == "" else 1
            #,calc_bin(cabin,10,30)
            #,cabin
            ,len(row[8].split(' '))
            ,embarked
    ]

#0 pclass,1 name,2 sex,3 age,4 sibsp,5 parch,6 ticket,7 fare,8 cabin,9 embarked
def prepare(raw_data, prep_vars):
    return [clean(row,prep_vars) for row in raw_data]
